import Model from '../../../models/Model'

export default function notExists(table: string, column: string, errorMessage?: string, clause?: any) {
  return (value) => {
    if (!value) {
      return false
    }
    const query = Model.knex()(table)
    if (clause) {
      clause(query)
    }
    if (typeof value === 'string') {
      value = value.trim()
    }
    return query
      .where(column, value)
      .count()
      .then((count) => {
        if (count[0]['count(*)'] > 0) {
          return errorMessage || 'The value is already taken'
        }
        return false
      })
  }
}
