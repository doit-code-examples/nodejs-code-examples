import HttpError from './HttpError'

export default class NotFound extends HttpError {
  constructor(message?: string, details?: object) {
    super(message || 'Requested resource not found', 404, details)
  }
}
