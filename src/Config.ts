export default class Config {
  logLevel = 40

  httpPort = 8000

  constructor(config = {}) {
    Object.assign(this, config)
  }

  rewriteFromEnvironment() {
    if (this.isEnvVariableSet('LOG_LEVEL')) {
      this.logLevel = Number(process.env.LOG_LEVEL)
    }
  }

  isEnvVariableSet(variable: string) {
    return process.env.hasOwnProperty(variable)
      && process.env[variable] !== ''
  }
}
