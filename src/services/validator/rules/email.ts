import { string } from 'yup'

export default function email(value) {
  return !value || string()
    .email()
    .isValidSync(value)
    ? false
    : 'The value should be a valid email'
}
