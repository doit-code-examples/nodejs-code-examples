export default function notEmpty(value) {
  return value === null
    || value === ''
    || Array.isArray(value) && value.length < 1
    ? 'The value should not be empty'
    : false
}
