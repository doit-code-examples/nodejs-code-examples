import { Request, Response, NextFunction } from 'express'

export default function catching(handler: CallableFunction) {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      await handler(req, res, next)
    } catch (e) {
      next(e)
    }
  }
}
