type Result = false | string
type RuleInterface = ((value: any, field: string, fields: Record<string, any>) => Result | Promise<Result>) | object
