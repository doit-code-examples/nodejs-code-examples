import { Model as BaseModel } from 'objection'

export default class Model extends BaseModel {
  static useLimitInFirst = true
}
