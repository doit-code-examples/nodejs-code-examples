import UnprocessableEntity from '../../errors/UnprocessableEntity'
import { Request as HttpRequest, Response, NextFunction } from 'express'
import Request from '../requests/Request'

export default function request<TRequest extends Request>(RequestClass: new (...args: any[]) => TRequest) {
  return async (req: HttpRequest, _: Response, next: NextFunction) => {
    try {
      const result = await (new RequestClass(req)).validate()
      if (result) {
        next(new UnprocessableEntity(result))
        return
      }
    } catch (e) {
      next(e)
      return
    }
    next()
  }
}
