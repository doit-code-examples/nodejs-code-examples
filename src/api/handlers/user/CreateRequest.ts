import email from '../../../services/validator/rules/email'
import notExists from '../../../services/validator/rules/notExists'
import Request from '../../requests/Request'
import length from '../../../services/validator/rules/length'
import notEmpty from '../../../services/validator/rules/notEmpty'
import required from '../../../services/validator/rules/required'
import User from '../../../models/User'

export default class CreateRequest extends Request {
  rules(): Record<string, RuleInterface[]> {
    return {
      name: [required, notEmpty, length(1, 80)],
      email: [required, email, notExists(User.tableName, 'email')]
    }
  }
}
