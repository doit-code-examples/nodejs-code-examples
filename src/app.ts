require('dotenv').config()

import http from './http'
import Config from './Config'
import os from 'os'
import cluster from 'cluster'
import Logger from './Logger'
import { init } from './store/db'

const config = new Config()
config.rewriteFromEnvironment()
const logger = new Logger(config.logLevel)

function launchHttp() {
  init()
  const numCPUs = Math.max(1, os.cpus().length - 2)
  let forksCount = 0
  if (process.env.NODE_ENV === 'production' && cluster.isMaster) {
    for (let i = 0; i < numCPUs; i++) {
      cluster.fork()
      forksCount++
    }

    cluster.on('exit', (_, code) => {
      if (code) {
        cluster.fork()
        return
      }
      forksCount--
      if (forksCount < 1) {
        logger.warning('All http workers have been stopped. Closing main process')
        process.exit()
      }
    })
  } else {
    http(config, logger).listen(config.httpPort, '0.0.0.0')
  }
}

function printHelp() {
  process.stdout.write(`cli [command]
Commands:
  http: launch a http server
`)
}

function main(args: string[]) {
  switch (args[0]) {
    case 'http': {
      launchHttp()
      break
    }

    default:
      printHelp()
      break
  }
}

main(process.argv.slice(2))
