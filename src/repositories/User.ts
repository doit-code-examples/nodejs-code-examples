import Model from '../models/User'
import Repository from './Repository'

export default class User extends Repository<Model> {
  model(): typeof Model {
    return Model
  }

  search(term: string) {
    this.getQuery().where(query => {
      query
        .where('User.name', 'like', `%${term}%`)
    })
    return this
  }

  filter(queryParams: { [k: string]: any }) {
    const { term } = queryParams
    if (term) {
      this.search(term)
    }
    return this
  }
}
