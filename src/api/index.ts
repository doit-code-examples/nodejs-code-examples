import { Router, Response } from 'express'
import user from './handlers/user'

export default function api(): Router {
  const router = Router()
  router.get('/health', (_, res: Response) => {
    res.send({
      status: 'ok'
    })
  })
  router.use('/users', user())
  return router
}
