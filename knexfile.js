require('dotenv').config()

const connection = {
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  charset: 'utf8mb4',
  multipleStatements: true
}

module.exports = {
  development: {
    client: 'mysql2',
    connection,
    migrations: {
      directory: './etc/migrations'
    },
    seeds: {
      directory: './etc/seeds'
    }
  },

  production: {
    client: 'mysql2',
    connection,
    migrations: {
      directory: './etc/migrations'
    },
    seeds: {
      directory: './etc/seeds'
    },
    pool: {
      min: 2,
      max: 10
    },
  }
}
