export default function array(value) {
  return typeof value === 'undefined' || Array.isArray(value)
    ? false
    : 'The value should be an array'
}
