import { Model } from 'objection'
import Knex from 'knex'

export function init(config?: Knex.Config & { pool?: Knex.Config['pool'] & { propagateCreateError?: boolean } }) {
  const knex = Knex({
    client: 'mysql2',
    connection: {
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      charset: 'utf8mb4',
    },
    pool: {
      min: 0,
      propagateCreateError: false,
    },
    migrations: {
      directory: './etc/migrations'
    },
    seeds: {
      directory: './etc/seeds'
    },
    ...config
  } as any)
  Model.knex(knex)
  return knex
}
