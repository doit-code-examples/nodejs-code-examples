interface ErrorInterface {
  // a HTTP status code
  statusCode: number;

  // additional info
  details?: object;
}
