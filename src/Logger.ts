const SeverityLevels = {
  DEBUG: 10,
  INFO: 20,
  WARNING: 30,
  ERROR: 40,
}

export default class Logger {
  severity: number

  constructor(severity: number) {
    this.severity = severity
  }

  debug(message: string, details: any = null) {
    if (this.severity <= SeverityLevels.DEBUG) {
      this.output(SeverityLevels.DEBUG, message, details)
    }
  }

  info(message: string, details: any = null) {
    if (this.severity <= SeverityLevels.INFO) {
      this.output(SeverityLevels.INFO, message, details)
    }
  }

  error(message: string, details: any = null) {
    if (this.severity <= SeverityLevels.ERROR) {
      this.output(SeverityLevels.ERROR, message, details)
    }
  }

  warning(message: string, details: any = null) {
    if (this.severity <= SeverityLevels.WARNING) {
      this.output(SeverityLevels.WARNING, message, details)
    }
  }

  output(_: number, message: string, details?: Record<string, any> | Error & Record<string, any>) {
    if (details) {
      if (details instanceof Error) {
        const object = Object.getOwnPropertyNames(details)
          .reduce((acc: Record<string, any>, key) => {
            // suppress TS warnings
            acc[key] = details ? details[key] : void 0
            return acc
          }, {})
        details = object
      }
      message += ` ${JSON.stringify(details)}`
    }
    console.log(message)
  }
}
