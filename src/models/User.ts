import Model from './Model'

export default class User extends Model {
  static tableName = 'User'

  id: number

  email: string

  name: string
}
