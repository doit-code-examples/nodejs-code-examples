import { Request as HttpRequest } from 'express'
import validate, { ValidationResult } from '../../services/validator/validate'

export default abstract class Request {
  request: HttpRequest

  constructor(request: HttpRequest) {
    this.request = request
  }

  validate(): Promise<ValidationResult> {
    return validate(
      this.rules(),
      { ...this.request.query, ...(this.request.body || {}) }
    )
  }

  abstract rules(): Record<string, RuleInterface[]>
}
