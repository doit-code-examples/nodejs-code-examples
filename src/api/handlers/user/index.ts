import { Router } from 'express'
import Controller from './Controller'
import request from '../../middlewares/request'
import catching from '../../middlewares/catching'
import CreateRequest from './CreateRequest'

export default function user() {
  const router = Router()
  const controller = new Controller()

  router.post('/', request(CreateRequest), catching(controller.create))
  router.get('/:userId', catching(controller.get))

  return router
}

