import { Request, Response } from 'express'
import User from '../../../models/User'
import UserRepository from '../../../repositories/User'
import NotFound from '../../../errors/NotFound'

export default class Controller {
  async create(req: Request, res: Response) {
    const { name, email } = req.body
    const user = await User.query()
      .insert({
        name,
        email
      })
    res
      .status(201)
      .send({
        user
      })
  }

  async get(req: Request, res: Response) {
    const { userId } = req.params
    const user = await new UserRepository()
      .getQuery()
      .findById(userId)
    if (!user) {
      throw new NotFound()
    }
    res
      .send({
        user
      })
  }
}
