export default function string(value) {
  return typeof value === 'string'
    ? false
    : 'The value should be a string'
}
