
export default class ErrorPresenter {
  error: Record<string, any> & ErrorInterface

  code?: number

  constructor(error: Record<string, any> & ErrorInterface, code?: number) {
    this.error = error
    this.code = code
  }

  present(data?: Record<string, any>): object {
    const result = {
      message: this.error.message,
      ...data
    } as any
    if (this.code) {
      result.code = this.code
    }
    return result
  }
}
