export default class HttpError extends Error implements ErrorInterface {
  statusCode: number

  details?: object

  constructor(message: string, statusCode: number, details?: object) {
    super(message)
    this.statusCode = statusCode
    this.details = details
  }
}
