import { string } from 'yup'

export default function length(min: number, max?: number) {
  return value => {
    if (typeof value !== 'string') {
      return false
    }
    const stringValidator = string()
    if (!stringValidator.min(min).isValidSync(value)) {
      return `The value must have at least ${min} characters`
    }
    if (max && !stringValidator.max(max).isValidSync(value)) {
      return `The value must have at most ${max} characters`
    }
  }
}
