import {
  Model,
  OrderByDirection,
  PageQueryBuilder,
  QueryBuilder,
  TransactionOrKnex,
  QueryBuilderType,
} from 'objection'

const DEFAULT_PAGE_LIMIT = 15

export default abstract class Repository<TModel extends Model> {
  protected allowedSortingFields: string[] = []

  private query?: QueryBuilder<TModel, any>

  abstract model(): { new(): TModel } | any

  setSortingFields(fields: string[]) {
    this.allowedSortingFields = fields
  }

  getQuery<QB extends QueryBuilder<TModel, any>>(
    trxOrKnex?: TransactionOrKnex
  ): QB {
    if (!this.query) {
      this.query = this.model().query(trxOrKnex)
    }
    return this.query as QB
  }

  paginate(
    page: number,
    perPage: number = DEFAULT_PAGE_LIMIT
  ): this & {
    getQuery(
      trxOrKnex?: TransactionOrKnex
    ): PageQueryBuilder<QueryBuilderType<TModel>>;
  } {
    this.getQuery().page(
      page > 0 ? page - 1 : 0,
      perPage
    )
    return this as this & {
      getQuery(
        trxOrKnex?: TransactionOrKnex
      ): PageQueryBuilder<QueryBuilderType<TModel>>;
    }
  }

  sort(column: string, order: OrderByDirection = 'asc'): this {
    if (!this.allowedSortingFields.includes(column)) {
      throw new RangeError(`Sorting by the field ${column} is not allowed`)
    }
    this.getQuery().orderBy(column, order)
    return this
  }

  listing(queryParams: Record<string, string>): this & {
    getQuery(
      trxOrKnex?: TransactionOrKnex
    ): PageQueryBuilder<QueryBuilderType<TModel>>;
  } {
    const { sort } = queryParams
    if (sort) {
      const multiple = sort.split(',')
      multiple.forEach((item) => {
        const pieces = item.split(' ')
        const column = pieces[0]
        const order = pieces[1]
        this.sort(column, order as OrderByDirection)
      })
    }

    const page = queryParams.page ? Number(queryParams.page) : 1
    let limit: number | undefined =
      queryParams.limit && !isNaN(Number(queryParams.limit))
        ? Number(queryParams.limit)
        : 0
    if (limit < 1 || limit > 100) {
      limit = void 0
    }

    this.paginate(page, limit)

    return this as this & {
      getQuery(
        trxOrKnex?: TransactionOrKnex
      ): PageQueryBuilder<QueryBuilderType<TModel>>;
    }
  }
}
