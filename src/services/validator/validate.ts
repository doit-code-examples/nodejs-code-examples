
export type ValidationResult = Record<string, string | object> | void

export default async function validate(
  schema: Record<string, any>,
  fields: Record<string, any>
): Promise<ValidationResult> {
  for (const fieldName of Object.keys(schema)) {
    for (const rule of schema[fieldName]) {
      if (typeof rule === 'object') {
        if (!fields[fieldName]) {
          continue
        }
        if (Array.isArray(fields[fieldName])) {
          for (const data of fields[fieldName]) {
            const error = await validate(rule, data)
            if (error) {
              return {
                [fieldName]: { [fields[fieldName].indexOf(data)]: error }
              }
            }
          }
        } else {
          const error = await validate(rule, fields[fieldName])
          if (error) {
            return {
              [fieldName]: error
            }
          }
        }
        continue
      }

      let value
      if (fields && Object.prototype.hasOwnProperty.call(fields, fieldName)) {
        value = fields[fieldName]
      }

      const error = await rule(value, fieldName, fields)
      if (error) {
        return { [fieldName]: error }
      }
    }
  }
}
