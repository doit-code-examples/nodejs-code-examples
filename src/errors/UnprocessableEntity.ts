import HttpError from './HttpError'

export default class UnprocessableEntity extends HttpError {
  constructor(details?: object) {
    super('An input data do not meet requirements', 422, details)
  }
}
