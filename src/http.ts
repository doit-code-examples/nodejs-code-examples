import express, { Express, Request, Response, NextFunction } from 'express'
import api from './api'
import Config from './Config'
import ErrorPresenter from './presenters/ErrorPresenter'
import Logger from './Logger'

export default function http(_: Config, logger: Logger): Express {
  const app = express()
  app.disable('x-powered-by')
  app.use(express.json())

  app.use('/api', api())
  app.use((_, res) => {
    res.status(404).send({ message: 'Not found' })
  })
  app.use((err: Record<string, any> & ErrorInterface, request: Request, res: Response, _: NextFunction) => {
    const status = err.statusCode || 500
    if (status === 500) {
      logger.error(`Error on ${request.method} ${request.path}`, err)
      const message = err.message
      err.message = 'Unable to process request. Please try later'
      if (process.env.NODE_ENV !== 'production') {
        err.details = { error: message }
      }
    }
    res
      .status(status)
      .json((new ErrorPresenter(err)).present(err.details))
  })
  return app
}
