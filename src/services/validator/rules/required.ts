export default function required(value) {
  return typeof value !== 'undefined' ? false : 'This field is required'
}
