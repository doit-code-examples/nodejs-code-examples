
exports.up = async function(knex) {
  await knex.schema.createTable('User', table => {
    table.increments('id')
    table.string('email').notNullable()
    table.string('name').notNullable()
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('User')
};
